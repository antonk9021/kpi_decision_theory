# -*- coding: utf-8 -*-
"""
Application entry point.

Example:
    $ python crops.pyw
"""
import sys
import models
import controllers as ctrls
#from views import window


# def main(window=window):
#     """
#     The main function.
#     :param window: PySimpleGui.Window instance.
#     """
#     while True:
#         event, values = window.read()
#         if callable(event):
#             event()
#         if event == "-LINEAR-":
#             window["-ALPHA-"].update(disabled=True)
#         if event == "-FUZZY-":
#             window["-ALPHA-"].update(disabled=False)
#         if event == "-SOLVE-":
#             for value in values:
#                 if isinstance(value, str) and value.startswith('a_'):
#                     print({value: values[value]})
#         if event in [None, "Exit", "Cancel", "Quit", WIN_CLOSED]:
#             break
#     window.close()

from PyQt5.QtWidgets import QApplication


def main() -> None:
    """
    Main function.
    """
    app = QApplication(sys.argv)
    model = models.Solver()
    controller = ctrls.MainController(model)

    app.exec()


if __name__ == '__main__':
    main()
