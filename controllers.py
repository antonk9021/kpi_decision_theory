
import numpy as np

import views
from typing import Iterable, Iterator, List, Tuple
import models


class MatrixMaker(object):  # TODO: Consider transferring this class to util.py
    """
    Class for transforming equations systems into constraint matrices.
    """
    @classmethod
    def make_matrix(cls, A: np.ndarray, b: np.ndarray, c: np.ndarray):

        # Appending identity matrix of the slack variables:
        slack_vars = np.eye(A.shape[0]*2)

        # Appending x_jk variables:
        squares = np.column_stack([np.eye(b.shape[0]) for i in range(b.shape[0])])
        coeffs = np.concatenate(([0], c, np.zeros(A.shape[0]*2)))

        # TODO: Fix stacking kludge below
        return np.row_stack((coeffs, np.column_stack((np.row_stack((A, np.column_stack((b, squares)))), slack_vars))))


class MainController(object):
    """
    Main controller class aimed to interact with the user.
    """
    def __init__(self, model: models.Solver):
        self.model = model
        self.view = views.MainView(self, self.model)

        self.view.show()

    @staticmethod
    def _transform_A(A: dict, j: int) -> Iterator[List[float]]:
        """
        Transform input A_j and a_jk row into a transposed vector.
        :param A: A_j and a_jk row contents.
        :param j: Row index.
        """
        # Transform `greater then` constraint into negative `less than`:
        sign = lambda x: -float(x) if A["constraint"] == '≥' else float(x)

        # If row and column indices are equal, return respective transformed value:
        for k, element in enumerate(range(len(A['a_jk']))):  # TODO: Fix this kludge
            yield [sign(a) if k == j else 0 for a in A['a_jk']]


    def _process_input(self, view_input: dict) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        transformed = lambda content, row_number: list(self._transform_A(content, row_number))
        A_j = [-float(j['A_j']) if j['constraint'] == '≥' else float(j['A_j']) for j in view_input['A']]
        a_j = np.column_stack((transformed(content=row_content, row_number=j) for j, row_content in enumerate(view_input['A'])))
        A = np.column_stack((A_j, a_j))
        b = np.array([float(constant) for constant in view_input['b']])
        c = np.array([float(coeff) for coeff in view_input['c']])

        return A, b, c

    def process(self, view_input: dict) -> None:
        processed = self._process_input(view_input)  # FIXME: Remove odd key `minimize`.
        matrix = MatrixMaker.make_matrix(*processed)

        self.model.optimized = {"matrix": matrix, "minimize": view_input["minimize"]}
