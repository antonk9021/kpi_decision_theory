# -*- coding: utf-8 -*-
"""
Exceptions raised on simplex algorithm-related errors.
"""

NoSolution = type("NoSolution", (BaseException,), {"message": "No feasible solution found for this function."})
ShapeError = type("ShapeError", (BaseException,), {"message": "Wrong matrix shape."})
