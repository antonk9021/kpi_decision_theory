# -*- coding: utf-8 -*-
"""
Views for interaction with the user.
"""
from __future__ import annotations
import itertools
from typing import Iterator, Iterable, List, Dict, TYPE_CHECKING
from PyQt5 import QtCore, QtGui, QtWidgets
import models
from views_templates import Ui_MainWindow
if TYPE_CHECKING:
    from controllers import MainController


class ErrorMessageBox(QtWidgets.QMessageBox):
    """
    Pops up on errors.
    """
    def __init__(self, msg: str):
        super().__init__()
        self.setWindowTitle("Помилка")
        self.setText(msg)
        self.setIcon(self.Critical)


class MainView(QtWidgets.QMainWindow): #, util.AbstractObserver, metaclass=util.ObserverMeta):
    """
    Main view class.
    """
    def __init__(self, controller: MainController, model: models.Solver, parent=None):  # TODO: Use facade pattern to change Simplex for a generalized solver
        super(QtWidgets.QMainWindow, self).__init__(parent)
        self.controller = controller
        self.model = model
        self.ui = Ui_MainWindow()

        self.ui.setupUi(self)
        self.model.add_observer(self)
        self.set_bindings()

    def set_bindings(self) -> None:
        """
        Connect view events with respective functions.
        """
        self.ui.solveButton.clicked.connect(self.get_input)
        self.ui.fuzzy.clicked.connect(self.activate_alpha_widget)
        self.ui.linear.clicked.connect(self.deactivate_alpha_widget)

    def activate_alpha_widget(self) -> None:
        """
        Activate alpha widget when `fuzzy` optimization is chosen.
        """
        self.ui.alpha.setEnabled(True)
        self.ui.alpha_min.setEnabled(True)
        self.ui.alpha_max.setEnabled(True)

        if any([not x.isEnabled() for x in [self.ui.opti_min, self.ui.opti_max]]):
            self.activate_optimization_widget()

    def deactivate_alpha_widget(self) -> None:
        """
        Deactivate alpha widget when `linear` optimization is chosen.
        """
        self.ui.alpha.setEnabled(False)
        self.ui.alpha_min.setEnabled(False)
        self.ui.alpha_max.setEnabled(False)

        if any([not x.isEnabled() for x in [self.ui.opti_min, self.ui.opti_max]]):
            self.activate_optimization_widget()

    def activate_optimization_widget(self) -> None:
        """
        Activate optimization switches.
        """
        self.ui.opti_min.setEnabled(True)
        self.ui.opti_max.setEnabled(True)

    def model_changed(self) -> None:
        """
        Displays result when model is ready with the calculation.
        """
        self.ui.result.setText(','.join(self.model.optimized))

    @property
    def constraints(self) -> List[str]:
        return [self.ui.a_constr_1, self.ui.a_constr_2, self.ui.a_constr_3, self.ui.a_constr_4]

    def _process_a_jk(self, row: int) -> Iterator[str]:
        for column in range(4):
            yield self.ui.a_jk.model().index(row, column).data()

    def _process_A_j(self, row: int) -> str:
        return self.ui.A_j.model().index(row, 0).data()

    @staticmethod
    def _unpack_table(table, columns: int, rows: int) -> Iterator[str]:
        for row, column in itertools.product(range(rows), range(columns)):
            yield table.model().index(row, column).data()

    def _get_a_jk(self) -> Iterator[Dict[str, Iterable]]:
        for row, constraint in zip(range(4), self.constraints):
            yield {"A_j": self._process_A_j(row),
                   "a_jk": list(self._process_a_jk(row)),
                   "constraint": constraint.currentText()}

    def _get_c_jk(self) -> Iterator[str]:
        return self._unpack_table(table=self.ui.c_jk, rows=4, columns=4)

    def _get_b_k(self) -> Iterator[str]:
        return self._unpack_table(table=self.ui.b_k, rows=4, columns=1)

    def get_input(self) -> None:
        if not any([x.isChecked() for x in [self.ui.linear, self.ui.fuzzy]]):
            ErrorMessageBox("Будь ласка, оберіть вид задачі").exec_()
        else:
            if not any([x.isChecked() for x in [self.ui.opti_min, self.ui.opti_max]]):
                ErrorMessageBox("Будь ласка, оберіть вид оптимізації").exec_()
            else:
                result = {
                    "A": list(self._get_a_jk()),
                    "b": self._get_b_k(),
                    "c": self._get_c_jk(),
                    "linear": self.ui.linear.isChecked(),
                    "minimize": self.ui.opti_min.isChecked(),
                    "alpha": self.ui.alpha.value() / 10
                }
                try:
                    self.controller.process(result)
                except (ValueError, TypeError) as e:
                    ErrorMessageBox(str(e)).exec_()
