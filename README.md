# README #

This is a README for the NTUU "KPI" Course project on decision theory (linear optimization plus fuzzy logic).

### The Course project ###

The Course project consists of two tasks. The 1st one relates to the linear optimization, while the 2nd refers to the fuzzy logic.

### Requirements ###
* Python 3.6+
* Packages from `requirements.txt`

### Installation and set up ###

* Create virtual environment if desired (see [official documentation](https://docs.python.org/3.6/library/venv.html) for the details)
    * Activate the environment
    
* Install required packages with `pip install -r requirements.txt`
* Launch with `python crops.pyw`
