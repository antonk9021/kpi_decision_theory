# -*- coding: utf-8 -*-
"""
Views for interaction with the user.
"""
import PySimpleGUI as sg
from typing import Dict, Any
from definitions import CELL_SIZE, CELL_PADDING, HEADER_PROPS


# class MainWindow(sg.Window):
#     """
#     Main application window class.
#     """
#     def __init__(self, title, layout, *args, **kwargs):
#         super(MainWindow, self).__init__(title=title, layout=layout, *args, **kwargs)


def help_message(window=None) -> sg.Popup:  # TODO: Consider better solution instead of this kludge
    """
    Help message popup.
    """
    return sg.Popup("Довідка", keep_on_top=True)


def toggle_alpha_slider(window: sg.Window) -> None:
    """
    Enabling alpha slider in fuzzy case and disabling in linear one.
    :param window: Main window instance.
    """
    window["-ALPHA-"].update(disabled=not(window["-ALPHA-"].Disabled))


"""
Markup items to be wrapped into respective PySimpleGui frames later on.
"""
# Crops yield table: a_jk
cy_headers = [[sg.Text(f"k={i + 1}", **HEADER_PROPS) for i in range(4)]]
cy_rows = [[sg.Input(size=CELL_SIZE, pad=CELL_PADDING, key=f"a_{row}{column}") for column in range(1, 4+1)] for row in range(1, 4+1)]

# Net cost table: c_jk
nc_headers = [[sg.Text(f"k={i+1}", **HEADER_PROPS) for i in range(4)]]
nc_rows = [[sg.Input(size=CELL_SIZE, pad=CELL_PADDING, key=f"c_{row}{column}") for column in range(1, 4+1)] for row in range(1, 4+1)]

# Demand column: A_j
d_column = [[sg.Input(size=CELL_SIZE, pad=CELL_PADDING, key=f"A_{j}")] for j in range(1, 4+1)]

# Plots areas row: b_k
area_row = [[sg.Input(size=CELL_SIZE, pad=CELL_PADDING, key=f"b_{k}") for k in range(1, 4+1)]]

# Radio button for switching between linear and fuzzy optimizations
fuzzy_linear_radio = [
    [sg.Radio(text="ЛП", group_id="fuzzy", enable_events=True, key="-LINEAR-"),
     sg.Radio(text="НМП", group_id="fuzzy", enable_events=True, key="-FUZZY-")]
]

# Radio button for switching between maximization and minimization
opt_radio = [[sg.Radio(text="max", group_id="optimization"), sg.Radio(text="min", group_id="optimization")]]

# Slider for regulating the alpha-level for the fuzzy problem
alpha_slider = [
    [sg.Slider(range=(.5, .9), orientation="horizontal", resolution=.1, default_value=.5, key="-ALPHA-", disabled=True)]
]

# Help button
help_button = sg.Button(button_text="Довідка", key=help_message)

# .xlsx upload button
upload_button = sg.FileBrowse(button_text="Завантажити .xlsx")

# Clear all button
clear_button = sg.Button(button_text="Очистити введене")

# Save button
save_button = sg.FileSaveAs(button_text="Зберегти результат")

# Submit button
submit_button = sg.Button(button_text="Розв\'язати", key="-SOLVE-")

"""
Frame wrappers for the above markup items. 
"""
crop_yield_frame = sg.Frame(layout=cy_headers + cy_rows, title="Врожайність")
net_cost_frame = sg.Frame(layout=nc_headers + nc_rows, title="Собівартість")
demand_frame = sg.Frame(layout=d_column, title="Потреба")
area_frame = sg.Frame(layout=area_row, title="Площа")
fuzzy_or_linear_frame = sg.Frame(layout=fuzzy_linear_radio, title="Вид задачі")
optimization_frame = sg.Frame(layout=opt_radio, title="Вид оптимізації")
alpha_frame = sg.Frame(layout=alpha_slider, title="Ступінь α")


layout = [
    [help_button, upload_button, clear_button, save_button],
    [crop_yield_frame, net_cost_frame] + [demand_frame],
    [area_frame],
    [fuzzy_or_linear_frame, optimization_frame, alpha_frame],
    [submit_button],
]

window = sg.Window("Розрахунок посівних площ", layout=layout, return_keyboard_events=True, resizable=True)
window.Finalize()
