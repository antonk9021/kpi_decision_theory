# -*- coding: utf-8 -*-
"""
Linear optimization and fuzzy logic classes.
"""

from abc import abstractmethod
import numpy as np
from typing import Tuple, List, Iterator
from PyQt5.QtCore import QObject, pyqtSignal


class BaseModel(QObject):
    """
    Model interface for interaction with other system layers.
    """
    optimized = pyqtSignal()

    def __init__(self, matrix, *args, **kwargs):
        super(BaseModel, self).__init__(*args, **kwargs)
        self.extended_matrix = matrix[1:]
        self.optimized.connect(self.optimize)

    def __getitem__(self, item):
        return self.extended_matrix[item]

    def __setitem__(self, key, value):
        self.extended_matrix[key] = value

    def __str__(self):
        return f"The tableau:\n {self[:, 1:]}\n" \
               f"\nThe constants: {self[:, 0]}\n" \
               f"\nObjective function coefficients: {np.array([i for i in self.c if i != 0])}" \
               f"\nDeltas: {self.deltas}"

    @property
    def shape(self) -> Tuple[int]:
        """
        Simplex tableau shape getter.
        """
        return self.extended_matrix.shape

    def minimize(self):
        return self.optimize(minimize=True)

    def maximize(self):
        return self.optimize(minimize=False)

    @abstractmethod
    def optimize(self, minimize: bool) -> None:
        pass


class Simplex(BaseModel):
    """
    Class for solving linear optimization problem through simplex method.
    """
    def __init__(self, matrix: np.ndarray):
        """
        :param matrix: Full initial matrix with constraints and constants.
        """
        super(Simplex, self).__init__(matrix)
        self.c = matrix[0]
        self.all_indices = [index for index in range(self.c.shape[0] - 1)]
        self.base_indices = [index - 1 for index in range(matrix.shape[1]) if index != 0 and matrix[0, index] == .0]
        # self.free_indices = [index - 1 for index in range(matrix.shape[1]) if index != 0 and matrix[0, index] != .0]
        self.result = {"plan": {}, "obj_func": None, "deltas": []}

    @staticmethod
    def _max_ratio(base_column: np.ndarray, pivot_column: np.ndarray) -> int:
        """
        Calculates maximum ratio between the base_indices and pivot columns.
        :param base_column: The base_indices column.
        :param pivot_column: Current pivot column.
        """
        pivot_column[pivot_column < 0] = np.inf  # FIXME: Fix zero division kludges.
        with np.errstate(all="ignore"):
            ratio = base_column / pivot_column
            ratio[np.logical_or(ratio <= 0, ratio == -np.inf, ratio != ratio)] = np.inf

        return np.where(ratio == min(ratio))[0][0]

    def _swap_terms(self, base_index: int, free_index: int) -> None:
        """
        Removes indicated term from the base_indices and inserts free term instead.
        :param base_index: Index of the term to be removed from the base_indices.
        :param free_index: Index of free term to be inserted into the base_indices.
        """
        self.base_indices.pop(base_index)
        self.base_indices.insert(base_index, self.all_indices[free_index])

    def find_pivots(self, minimize: bool) -> Tuple[int, int]:
        """
        Find pivot row and column indices.
        :param deltas: Deltas row to be checked for optimality.
        :param minimize: Sets whether objective function is being minimized or maximized.
        """
        # Find column index `j` with maximum absolute negative value:
        non_zeros = filter(lambda delta: delta != 0, self.deltas)
        # for non_zero in non_zeros:
        #     print(non_zero)
        extremum = max(non_zeros) if minimize else min(non_zeros)

        pivot_j = np.where(self.deltas == extremum)[0][0] + 1  # Because deltas subset used here does not include b vector
        # print(np.where(deltas == extremum))
        # Find row index `i` with minimum ratio between base_indices and pivot column values, where pivot column is > 0:
        pivot_i = self._max_ratio(np.copy(self[:, 0]), np.copy(self[:, pivot_j]))

        return pivot_i, pivot_j

    @property
    def base_terms(self) -> List:
        """
        Gets current base elements.  # FIXME: Not clear what it really does
        """
        return [self.c[index + 1] for index in self.base_indices]

    @property
    def deltas(self) -> Iterator[float]:
        """
        Gets deltas for current iteration.
        """
        return [sum(self[:, index] * self.base_terms) - value for index, value in enumerate(self.c)][1:]

    @property
    def obj_func(self) -> float:
        """
        Gets current objective function value
        """
        return sum(self[:, 0] * self.base_terms) - self.c[0]

    def gauss_eliminate(self, i: int, j: int) -> None:
        """
        Make Gaussian elimination for the given row and column.
        :param i: The row.
        :param j: The column.
        """
        self._swap_terms(i, j-1)

        if self[i][j] != 1:
            self[i] /= self[i][j]

        for number, row in enumerate(self):
            if number != i:
                row -= self[i] * row[j]

    def optimize(self, minimize=True) -> None:
        """
        Finds optimal plan if available.
        :param minimize: Sets whether objective function is being minimized or maximized.
        """
        while any([x < 0 for x in self[:, 0]]):  # While there is at least one negative constant
            # Get row with the largest (by absolute value) negative constant:
            pivot_i = np.where(self[:, 0] == min(self[:, 0]))[0][0]

            # If all ratios between delta[j] and respective j-column are negative, the system is insoluble:
            # if all([item/self.deltas[j] < 0 for item in self[pivot_i]]): #FIXME: won't work on maximization
            #     raise NoSolution

            # Get the largest (by absolute value) item in the row:
            pivot_j = np.where(abs(self[pivot_i]) == max(abs(self[pivot_i, 1:])))[0][0]

            # Make Gaussian elimination on retrieved item:
            self.gauss_eliminate(pivot_i, pivot_j)

        vector_criterion = lambda delta: delta > 0 if minimize else delta < 0

        while any(map(vector_criterion, self.deltas)):
            pivots = self.find_pivots(minimize)
            self.gauss_eliminate(*pivots)

        counter = 0
        for index in self.all_indices[:(len(self.all_indices) - len(self.base_indices))]:
            if index in self.base_indices:
                self.result["plan"][f"X{index+1}"] = self[counter, 0]
                counter += 1
            else:
                self.result["plan"][f"X{index+1}"] = 0
        self.result["obj_func"] = self.obj_func
        self.result["deltas"].extend(self.deltas)

        # self.optimized.emit()


if __name__ == '__main__':
    import os
    import json
    matrix_csv = os.path.join(os.getcwd(), "input", "matrix.csv")
    matrix = np.genfromtxt(matrix_csv, delimiter=',')
    simplex = Simplex(matrix)
    simplex.minimize()
    print(json.dumps(simplex.result, indent=4))
    # for item in simplex.minimize():
    #     print(item)
