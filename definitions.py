# -*- coding: utf-8 -*-
"""
Constants, directories, and other definitions.
"""

import os
from numpy import inf

INPUT_DIR = os.path.join(os.getcwd(), "input")

# M value for M-simplex:
M = inf

# Main window cells config
CELL_SIZE = (7, 1)
CELL_PADDING = (1, 1)
CELL_HEADER_SIZE = (6, 1)
CELL_HEADER_PADDING = (0, 0)
CELL_HEADER_FONT = 'Courier 11'

HEADER_PROPS = {"size": CELL_HEADER_SIZE}
